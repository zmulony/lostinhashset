package pl.edu.pwr.lostinhashset.entity;

public class User {

	private String name;
	private String surname;
	private int points;
	
	public User() {
		
	}
	
	static int hash = 0;
	@Override
    public int hashCode() {
        return hash++;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    public User(String name, String surname, int points) {
		this.name = name;
		this.surname = surname;
		this.points = points;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	
	
}
