package pl.edu.pwr.lostinhashset.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import pl.edu.pwr.lostinhashset.entity.User;
import pl.edu.pwr.lostinhashset.repository.UserRepository;
import pl.edu.pwr.lostinhashset.service.UserService;

public class DefaultUserService implements UserService {
	
	private UserRepository userRepository;

	@Override
	public List<User> getUsersSortedByName() {
		
		List<User> users = getUserRepository().getUsers();
		users.sort(new Comparator<User>() {

            @Override
            public int compare(User arg0, User arg1) {
                return arg0.getName().compareTo(arg1.getName());
            }
		    
        });
		// With lambda expression
		// users.sort((x, y) -> x.getName().compareTo(y.getName()));
		return users;
	}

	@Override
	public List<User> getUsersSortedByPoints() {

		List<User> users = getUserRepository().getUsers();
		Collections.sort(users, new Comparator<User>() {

            @Override
            public int compare(User arg0, User arg1) {
                return arg0.getPoints() - arg1.getPoints();
            }
            
        });
		// With lambda expression
		// Collections.sort(users, (x, y) -> x.getPoints() - y.getPoints());
		return users;
	}

	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

}
